# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "D:/Users/Bendou/CLionProjects/MathToolKitCPP/main.cpp" "D:/Users/Bendou/CLionProjects/MathToolKitCPP/cmake-build-debug/CMakeFiles/MathToolKit.dir/main.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../NAlgebra/header"
  "../NAnalysis/header"
  "../NGeometry/header"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "D:/Users/Bendou/CLionProjects/MathToolKitCPP/cmake-build-debug/NAnalysis/CMakeFiles/NAnalysis.dir/DependInfo.cmake"
  "D:/Users/Bendou/CLionProjects/MathToolKitCPP/cmake-build-debug/NGeometry/CMakeFiles/NGeometry.dir/DependInfo.cmake"
  "D:/Users/Bendou/CLionProjects/MathToolKitCPP/cmake-build-debug/NAlgebra/CMakeFiles/NAlgebra.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
